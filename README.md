#Apache Tom-Cat Web Applications GameSpeed

This is a web application written in Java using Servlets and JSP's.  It also uses SQL
to store product and user information.  

##Motivation

This is a web application for a fake electronics store.  It demonstates the use of Ajax 
auto fill search, as well as sql.  
 
##Installation
You must have JDK 1.7
Open your cammand prompt and cd into the bin directory of this project.  Type `env-setup-for-tomcat`
This will set all of the class paths for this application.  Next type `startup` and the 
apache server will start.  Open an internet browser and go to the url localhost/gamespeed.