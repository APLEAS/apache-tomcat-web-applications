<HTML>

  <%-- import any packages needed by the page --%>
    <%@ page import="products.GameConsole,products.*, products.ConsoleHashMap,products.ProductStore, java.util.Map,java.util.HashMap, java.io.*, java.util.*" %>

<%
		String name = null;
		String CategoryName = request.getParameter("maker");
		ConsoleHashMap cmap = new ConsoleHashMap();
		HashMap<String, GameConsole> hm = new HashMap<String, GameConsole>(); 	
		if(CategoryName==null){
			hm = ProductHashMap.consoles;
			//System.out.println("just got done from product store   !!!!!!!!!!!!!");
			name = "";
		}else{
			if(CategoryName.equals("microsoft")){
				hm.putAll(cmap.microsoft);
				name = cmap.string_microsoft;
				System.out.print("inside microsoft   =" + hm.size());
			}
			else if(CategoryName.equals("sony")){
				
				hm.putAll(cmap.sony);
				System.out.println("Inside sony  "  + hm.size());
				name = cmap.string_sony;
			}
			else if(CategoryName.equals("nintendo")){
				hm.putAll(cmap.nintendo);
				name = cmap.string_nintendo;
			}
		}

%>

<%@include file="global/site_header.html"%>




  <%@include file="global/site_sidebar.html"%>
  <div id='content'><div class='post'><h2 class='title meta'>
<a style='font-size: 24px;'><%=name%>Consoles</a>
</h2><div class='entry'><table id='bestseller'>
		
	<%
		int i = 1; int size= hm.size();
		for(Map.Entry<String, GameConsole> entry : hm.entrySet()){
			GameConsole console = entry.getValue();
			if(i%3==1){
				%><tr> 
				<%}%>
				
			<td><div id='shop_item'>
			<h3><%= console.getName()%></h3>
			<strong>$<%=console.getPrice()%></strong><ul>

			<li id="item"><img src="images/consoles/<%=console.getImage()%>" alt="" /></li>
			<li><form method="post" action="Cart.jsp">
					<input type="hidden" name="name" value="<%=console.getName()%>">
					<input type="hidden" name="type" value='console' >
					<input type="hidden" name="maker" value="<%=console.getRetailer()%>">
					<input type="hidden" name="access" value="">
					<input type="hidden" name="oid" value="<%=console.getId()%>">
					<input type="submit" class="btnbuy" value="Add To Cart" href="#"></input></form></li>
					<li><a class='btnbuy' href="AccessoryList.jsp?maker=<%=console.getRetailer()%>&console=<%=console.getName()%>" >View Accessories </a></li>
			<li><a class='btnreview' href="ReviewList.jsp?name=<%=console.getName()%>&maker=<%=console.getRetailer()%>&type=<%=console.getType()%>">Reviews</a></li>
			<li><a class='btnreview' href="WriteReview.jsp?name=<%=console.getName()%>&consoles&maker=<%=console.getRetailer()%>&type=<%=console.getType()%>">Write Review</a></li></ul></div></td>
			
			<%if(i%3==0 || i == size){%> </tr> <% }i++;}%>		
			
	</table></div></div></div>	

</HTML>
