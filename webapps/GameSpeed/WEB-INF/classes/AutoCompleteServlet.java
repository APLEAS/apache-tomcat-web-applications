
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import products.*;

import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author nbuser
 */
public class AutoCompleteServlet extends HttpServlet {

    private ServletContext context;
    private HashMap<String, Product> products = AllProductsMap.products;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.context = config.getServletContext();
    }

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String action = request.getParameter("action");
	
        String targetId = request.getParameter("id");
		String originalId = targetId;
        StringBuffer sb = new StringBuffer();
		String type = "";
		System.out.println("target id start   =  "   + targetId);

        if (targetId != null) {
            targetId = targetId.trim().toLowerCase();
        } else {
            context.getRequestDispatcher("/error.jsp").forward(request, response);
        }

        boolean namesAdded = false;
        if (action.equals("complete")) {

            // check if user sent empty string
            if (!targetId.equals("")) {

                Iterator it = products.keySet().iterator();

                while (it.hasNext()) {
                    String id = (String) it.next();
					System.out.println("this is the id   "  + id);
                    Product product = products.get(id);

                    if ( // targetId matches first name
                         product.getName().toLowerCase().startsWith(targetId) ||
                         // targetId matches last name
                         product.getRetailer().toLowerCase().startsWith(targetId) ||
                         // targetId matches full name
                         product.getName().toLowerCase().concat(" ")
                            .concat(product.getRetailer().toLowerCase()).startsWith(targetId)) {
System.out.println("we have a product match   "  + product.getName() + " : " +product.getId() + " : " + product.getRetailer());
                        sb.append("<product>");
                        sb.append("<id>" + product.getId() + "</id>");
                        sb.append("<retailer>" + product.getRetailer() + "</retailer>");
                        sb.append("<name>" + product.getName() + "</name>");
						 sb.append("<price>" + product.getPrice() + "</price>");
						  sb.append("<type>" + product.getType() + "</type>");
						   sb.append("<image>" + product.getImage() + "</image>");
                        sb.append("</product>");
                        namesAdded = true;
                    }
                }
            }

            if (namesAdded) {
				System.out.println("names have been added ");
                response.setContentType("text/xml");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().write("<products>" + sb.toString() + "</products>");
            } else {
                //nothing to show
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }

        if (action.equals("lookup")) {

 
 System.out.println("looking up targetId1  " + originalId);
 
            // put the target composer in the request scope to display 
            if ((originalId != null) && products.containsKey(originalId)) {
				Product p = products.get("Fifa 16");
				
				System.out.println("looking up targetId2  " + originalId);
				System.out.println("looking up name  " + p.getName());
				
                request.setAttribute("product", products.get(originalId));
				
                context.getRequestDispatcher("/product.jsp").forward(request, response);
            }
        }
    }
}
