package store;
import products.Game;
import java.util.*;
import java.io.*;
import java.io.File;

public class ProductStore{


// The known platforms

	public static HashMap<String, GameConsole> consoles = new HashMap<String, GameConsole>();







// UserInfo


public static boolean storeConsole(GameConsole console, boolean update) {
	
	boolean store = false;
 HashMap<String,GameConsole> mapInFile;
   
    try{
        File consoleData=new File("ConsoleData");
		BufferedReader br = new BufferedReader(new FileReader("ConsoleData"));
if(consoleData.exists() && !consoleData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(consoleData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,GameConsole>)ois.readObject();	
        ois.close();
        fis.close();
}else{ 
consoleData.createNewFile();
mapInFile = new HashMap<String, GameConsole>();
mapInFile.put(console.getName(), console);
	store = true;
}
   
		

		
			if(mapInFile.get(console.getName()) == null || update || store){
				store = true;
			mapInFile.put(console.getName(), console);
			} else{  System.out.println("this console is allready in the system  "  + console.getName());
			store = false;}
			
			if(store){
			try{	
		FileOutputStream fos=new FileOutputStream(consoleData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write console to file ..." + e);
	}
			}

    }catch(Exception e){System.out.println("Could NOT write console to file ..." + e);}
	
return store;
}

public static HashMap<String, GameConsole> getConsoleMap(){
 HashMap<String ,GameConsole> mapInFile = new HashMap<String, GameConsole>();
	 try{
        File consoleData=new File("ConsoleData");
		BufferedReader br = new BufferedReader(new FileReader("ConsoleData"));
if(consoleData.exists() && !consoleData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(consoleData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,GameConsole>)ois.readObject();	
        ois.close();
        fis.close();
		
}
	 }catch(Exception e){System.out.println("Could NOT read user to file ..." + e);}
	return mapInFile;
}

public static boolean updateConsole(String name, String update, int price) {

 HashMap<String,GameConsole> mapInFile = new HashMap<String, GameConsole>();

    try{
        File consoleData=new File("ConsoleData");
		BufferedReader br = new BufferedReader(new FileReader("ConsoleData"));
if(consoleData.exists() && !consoleData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(consoleData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,GameConsole>)ois.readObject();	
        ois.close();
        fis.close();
}	
			if(mapInFile.get(name) == null){
				System.out.println("this console does not exist  ");
			//	store = true;
			///mapInFile.put(console.getName(), console);
			} else if(update.equals("delete")){  
			mapInFile.remove(name);
			System.out.println("This console has been removed "  + name);
		}else if(update.equals("price")){
			GameConsole console = mapInFile.get(name);
			console.setPrice(price);
			mapInFile.put(console.getName(), console);
			System.out.println("This console has been updated "  + name);
			
		}
			try{	
		FileOutputStream fos=new FileOutputStream(consoleData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write console to file ..." + e);
			} 
    }catch(Exception e){System.out.println("Could NOT write console to file ..." + e);}
	
return true;
}

public static boolean changeConsolePrice(String name, int price) {
 HashMap<String,GameConsole> mapInFile = new HashMap<String, GameConsole>();

    try{
        File consoleData=new File("ConsoleData");
		BufferedReader br = new BufferedReader(new FileReader("ConsoleData"));
if(consoleData.exists() && !consoleData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(consoleData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,GameConsole>)ois.readObject();	
        ois.close();
        fis.close();
}	
			if(mapInFile.get(name) == null){
				System.out.println("this console does not exist  ");
			//	store = true;
			///mapInFile.put(console.getName(), console);
			} else{  
			GameConsole console = mapInFile.get(name);
			console.setPrice(price);
			mapInFile.put(console.getName(), console);
			System.out.println("This console has been removed "  + name);
			}
			try{	
		FileOutputStream fos=new FileOutputStream(consoleData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write console to file ..." + e);
			} 

    }catch(Exception e){System.out.println("Could NOT write console to file ..." + e);}
	
return true;
}



public static boolean storeAccessory(Accessory accessory, boolean update) {
	
	boolean store = false;
 HashMap<String,Accessory> mapInFile;
   
    try{
        File consoleData=new File("AccessoryData");
		BufferedReader br = new BufferedReader(new FileReader("AccessoryData"));
if(consoleData.exists() && !consoleData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(consoleData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,Accessory>)ois.readObject();	
        ois.close();
        fis.close();
}else{ 
consoleData.createNewFile();
mapInFile = new HashMap<String, Accessory>();
mapInFile.put(accessory.name, accessory);
	store = true;
}
   	
			if(mapInFile.get(accessory.name) == null || update || store){
				store = true;
			mapInFile.put(accessory.name, accessory);
			} else{  System.out.println("this accessory is allready in the system  "  + accessory.name);
			store = false;}
			
			if(store){
			try{	
		FileOutputStream fos=new FileOutputStream(consoleData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write Accessory to file ..." + e);
	}
			}    

    }catch(Exception e){System.out.println("Could NOT write Accessory to file ..." + e);}
	
return store;
}

public static boolean storeGame(Game game, boolean update) {
	boolean store = false;
 HashMap<String,Game> mapInFile;
   
    try{
        File gameData=new File("GameData");
		BufferedReader br = new BufferedReader(new FileReader("GameData"));
if(gameData.exists() && !gameData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(gameData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,Game>)ois.readObject();	
        ois.close();
        fis.close();
}else{ 
gameData.createNewFile();
mapInFile = new HashMap<String, Game>();
mapInFile.put(game.getName(), game);
	store = true;
}
   	
			if(mapInFile.get(game.getName()) == null || update || store){
				store = true;
			mapInFile.put(game.getName(), game);
			} else{  mapInFile.put(game.getName(), game);
			store = true;}
			
			if(store){
			try{	
		FileOutputStream fos=new FileOutputStream(gameData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write Game to file ..." + e);
	}
			}    

    }catch(Exception e){System.out.println("Could NOT write Game to file ..." + e);}
	
return store;
}

public static HashMap<String, Game> getGameMap(){
 HashMap<String ,Game> mapInFile = new HashMap<String, Game>();
	 try{
        File gameData=new File("GameData");
		BufferedReader br = new BufferedReader(new FileReader("GameData"));
if(gameData.exists() && !gameData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(gameData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,Game>)ois.readObject();	
        ois.close();
        fis.close();
		
}
	 }catch(Exception e){System.out.println("Could NOT read game hashmap file ..." + e);}
	return mapInFile;
}


}