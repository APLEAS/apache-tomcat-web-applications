package helper;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import classes.products;
import users.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Helper {
	HttpServletRequest req;
	PrintWriter pw;
	String url;
	HttpSession session; 

	public Helper(HttpServletRequest req) {
		this.req = req;
		this.session = req.getSession(true);
	}


	
	public void logout(){
		session.removeAttribute("username");
		session.removeAttribute("usertype");
	}
	
	public boolean isLoggedin(){
		if (session.getAttribute("username")==null)
			return false;
		return true;
	}
	
	public String username(){
		if (session.getAttribute("username")!=null)
			return session.getAttribute("username").toString();
		return null;
	}
	
	public String usertype(){
		if (session.getAttribute("usertype")!=null)
			return session.getAttribute("usertype").toString();
		return null;
	}
	
	public User getUser(){
		String usertype = usertype();
		HashMap<String, User> hm = new HashMap<String, User>();
		if (usertype.equals("customer")) {
			hm.putAll(UserHashMap.customer);
		} else if (usertype.equals("retailer")) {
			hm.putAll(UserHashMap.retailer);
		} else if (usertype.equals("manager")) {
			hm.putAll(UserHashMap.manager);
		}
		User user = hm.get(username());
		return user;
	}
	
	public ArrayList<OrderItem> getCustomerOrders(){
		ArrayList<OrderItem> order = new ArrayList<OrderItem>(); 
		if(OrdersHashMap.orders.containsKey(username()))
			order= OrdersHashMap.orders.get(username());
		return order;
	}
	

	
	public int CartCount(){
		if(isLoggedin())
		return getCustomerOrders().size();
		return 0;
	}
	
	

		
	public void storeProduct(String name,String type,String maker, String acc){
		System.out.println("name   ="  + name);
		if(!OrdersHashMap.orders.containsKey(username())){	
			ArrayList<OrderItem> arr = new ArrayList<OrderItem>();
			OrdersHashMap.orders.put(username(), arr);
		}
		
		ArrayList<OrderItem> orderItems = OrdersHashMap.orders.get(username());
		
		if(type.equalsIgnoreCase("consoles")){
			GameConsole console = null;
			if(maker.equalsIgnoreCase("microsoft")){
				console = ConsoleHashMap.microsoft.get(name);
			}
			else if(maker.equalsIgnoreCase("sony")){
				console = ConsoleHashMap.sony.get(name);
			}
			else if(maker.equalsIgnoreCase("nintendo")){
				console = ConsoleHashMap.nintendo.get(name);
			}else{
				HashMap<String, GameConsole> hm = new HashMap<String, GameConsole>();
				hm.putAll(ConsoleHashMap.microsoft);
				hm.putAll(ConsoleHashMap.sony);
				hm.putAll(ConsoleHashMap.nintendo);				
				console = hm.get(name);
				
			}
			OrderItem orderitem = new OrderItem(console.name, console.price, console.image, console.retailer);
			orderItems.add(orderitem);
		}
		if(type.equalsIgnoreCase("games")){
			Game game = null;
			if(maker.equalsIgnoreCase("electronicArts")){
				game = GameHashMap.electronicArts.get(name);
			}
			else if(maker.equalsIgnoreCase("activision")){
				game = GameHashMap.activision.get(name);
			}
			else if(maker.equalsIgnoreCase("takeTwoInteractive")){
				game = GameHashMap.takeTwoInteractive.get(name);
			}else{
				HashMap<String, Game> hm = new HashMap<String, Game>();
				hm.putAll(GameHashMap.electronicArts);
				hm.putAll(GameHashMap.activision);
				hm.putAll(GameHashMap.takeTwoInteractive);				
				game = hm.get(name);
			}
			OrderItem orderitem = new OrderItem(game.getName(), game.getPrice(), game.getImage(), game.getRetailer());
			orderItems.add(orderitem);
		}
		
		if(type.equalsIgnoreCase("tablets")){
			Tablet tablet = null;
			if (maker.equalsIgnoreCase("apple")) {
				tablet = TabletHashMap.apple.get(name);
			} else if (maker.equalsIgnoreCase("microsoft")) {
				tablet = TabletHashMap.microsoft.get(name);
			} else if (maker.equalsIgnoreCase("samsung")) {
				tablet = TabletHashMap.samsung.get(name);
			}else{
				HashMap<String, Tablet> hm = new HashMap<String, Tablet>();
				hm.putAll(TabletHashMap.apple);
				hm.putAll(TabletHashMap.microsoft);
				hm.putAll(TabletHashMap.samsung);				
				tablet = hm.get(name);
			}
			OrderItem orderitem = new OrderItem(tablet.getName(), tablet.getPrice(), tablet.getImage(), tablet.getRetailer());
			orderItems.add(orderitem);
		}
		
		if(type.equalsIgnoreCase("accessories")){
			GameConsole console = null;
			if(maker.equalsIgnoreCase("microsoft")){
				console = ConsoleHashMap.microsoft.get(acc);
				System.out.println("got my 360 "  + acc);
			}
			else if(maker.equalsIgnoreCase("sony")){
				console = ConsoleHashMap.sony.get(acc);
			}
			else if(maker.equalsIgnoreCase("nintendo")){
				console = ConsoleHashMap.nintendo.get(acc);
			}
				System.out.println("got inside accessory 1"  + name);
			Accessory accessory = console.getAccessory(name);
			 	System.out.println("got inside accessory 2  "   + accessory.getName());
			OrderItem orderitem = new OrderItem(accessory.getName(), accessory.getPrice(), accessory.getImage(), accessory.getRetailer());
				System.out.println("got inside accessory 3");
			orderItems.add(orderitem);
		}
		
	}
	
	public String currentDate(){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/YYYY");
		Date date = new Date();
		return dateFormat.format(date).toString(); 
	}
	
	public HashMap<String, GameConsole> getConsoles(){
			HashMap<String, GameConsole> hm = new HashMap<String, GameConsole>();
			hm.putAll(ConsoleHashMap.microsoft);
			hm.putAll(ConsoleHashMap.sony);
			hm.putAll(ConsoleHashMap.nintendo);
			return hm;
	}
	
	public HashMap<String, Game> getGames(){
		HashMap<String, Game> hm = new HashMap<String, Game>();
			hm.putAll(GameHashMap.electronicArts);
			hm.putAll(GameHashMap.activision);
			hm.putAll(GameHashMap.takeTwoInteractive);
			return hm;
	}
	
	public HashMap<String, Tablet> getTablets(){
			HashMap<String, Tablet> hm = new HashMap<String, Tablet>();
			hm.putAll(TabletHashMap.apple);
			hm.putAll(TabletHashMap.microsoft);
			hm.putAll(TabletHashMap.samsung);
			return hm;
	}
	
	public ArrayList<String> getProducts(){
		ArrayList<String> ar = new ArrayList<String>();
		for(Map.Entry<String, GameConsole> entry : getConsoles().entrySet()){			
			ar.add(entry.getValue().getName());
		}

		return ar;
	}
	
	public ArrayList<String> getProductsGame(){		
		ArrayList<String> ar = new ArrayList<String>();
		for(Map.Entry<String, Game> entry : getGames().entrySet()){
			ar.add(entry.getValue().getName());
		}
		return ar;
	}
	
	public ArrayList<String> getProductsTablets(){		
		ArrayList<String> ar = new ArrayList<String>();
		for(Map.Entry<String, Tablet> entry : getTablets().entrySet()){
			ar.add(entry.getValue().getName());
		}
		return ar;
	}
	
	

}
