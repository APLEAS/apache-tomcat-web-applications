package products;

import java.util.ArrayList;
import java.util.HashMap;

public class OrdersHashMap {

	public static HashMap<String, ArrayList<OrderItem>> orders = new HashMap<String, ArrayList<OrderItem>>();
	
	public OrdersHashMap() {
		
	}
public void deleteOrder(String user, String order){
	ArrayList <OrderItem> o = orders.get(user);
	for (int i = 0; i < o.size(); i++){
		System.out.println("order   "  + o.get(i).getName());
		if (o.get(i).getName().equals(order))
			o.remove(i);
	}
	orders.put(user, o);
}

public void addWarranty(String user, String order, double price){
	ArrayList <OrderItem> o = orders.get(user);
	for (int i = 0; i < o.size(); i++){
		System.out.println("order   "  + o.get(i).getName());
		if (o.get(i).getName().equals(order))
			o.get(i).setWarranty(price);
	}
	orders.put(user, o);
}

public boolean cancelOrder(String tracking){
	boolean success = false;
	//ArrayList <OrderItem> o = ShippedOrderStore.getShippedOrders(user);
	if (tracking != null){
		System.out.println("sending to be canceled   " + tracking);
	// success = ShippedOrderStore.shipOrder(user, o, true, order);
	SQLStoreShipments shipper = new SQLStoreShipments();
	 shipper.cancelOrder(tracking);
	}else {System.out.println("orders is null");}
	return false;  
}

public void shipOrder(User u, ArrayList<OrderItem> orders){
	System.out.println("inside ship orders");
//	ArrayList <OrderItem> o = ShippedOrderStore.getShippedOrders(user);
	SQLStoreShipments shipper = new SQLStoreShipments();  
	System.out.println("order size   "  + orders.size());
	for (OrderItem order: orders)
	shipper.addOrder(u.getId(), u.getName(), order.getPrice(), order.getImage(), order.getRetailer(), order.getType(), order.getDateOrdered(), order.getTracking(), order.getDeliveryDate(), order.getWarranty(), order.getName());
	//if (orders != null){
	//	ShippedOrderStore.shipOrder(user, orders, false, "");
	//}else {System.out.println("orders is null");}
}

}
