package products;

public class Game implements java.io.Serializable{
	private String name;
	private int price;
	private String image;
	private String retailer;
	private String condition;
	private int discount;
	private String id;
	private final String type = "game";
	
	public Game(String name, int price, String image, String retailer,String condition,int discount){
		this.name=name;
		this.price=price;
		this.image=image;
		this.condition=condition;
		this.discount = discount;
		this.retailer = retailer;
	}
	
	public Game(){
		
	}
	
	public void setId(String id){
		this.id = id;
	} 
	public String getId(){
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getRetailer() {
		return retailer;
	}
	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getEAWarranty(){
	return 5;
}

public int getActivisionWarranty(){
	return 5;
}

public int getTake2Warranty(){
	return 10;
}
public String getType(){
	return type;
}
	
}
