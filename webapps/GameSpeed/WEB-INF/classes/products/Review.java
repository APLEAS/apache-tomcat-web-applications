package products;

public class Review implements java.io.Serializable{
	public String name;
	public String category;
	public String price;
	public String retailer;
	public String retailerZip;
	public String retailerCity;
	public String retailerState;
	public String onSale;
	public String manuName;
	public String manuRebate;
	public String userName;
	public String userAge;
	public String userGender;
	public String userOccupation;
	public String rating;
	public String date;
	public String text;
	
	public Review() {
	}
		public void setName(String name) {
		this.name = name;
	}
public void setCategory(String category){
	this.category = category;
}
		public void setPrice(String price) {
		this.price = price;
	}
			public void setRetailer(String r) {
		this.retailer = r;
	}
				public void setRetailerZip(String zip) {
		this.retailerZip = zip;
	}
			public void setRetailerCity(String city) {
		this.retailerCity = city;
	}
	public void setRetailerState(String state){
		this.retailerState = state;
	}
	
	public void setOnSale(String sale){
		this.onSale = sale;
	}
	public void setManuName(String name){
		this.manuName = name;
	}
	
		public void setManuRebate(String rebate){
		this.manuRebate = rebate;
	}
	
	public void setUserName(String name){
		this.userName = name;
	}
	
	public void setUserAge(String age){
		this.userAge = age;
	}
	
	public void setUserGender(String gender){
		this.userGender = gender;
	}
	
	public void setUserOccupation(String o){
		this.userOccupation = o;
	}
	
	public void setRating(String r){
		this.rating = r;
	}
	
	public void setDate(String d){
		this.date = d;
	}
	public void setText(String text){
		this.text = text;
	}

}

	