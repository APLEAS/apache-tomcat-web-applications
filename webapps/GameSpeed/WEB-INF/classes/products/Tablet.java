package products;

public class Tablet implements java.io.Serializable{
	private String name;
	private int price;
	private String image;
	private String retailer;
	private String condition;
	private int discount;
	private String id;
	public String type = "tablet";
	
	public Tablet(String name, int price, String image, String retailer, String condition,int discount){
		this.name=name;
		this.price=price;
		this.image=image;
		this.condition=condition;
		this.discount = discount;
		this.retailer = retailer;
	}
	
	public Tablet(){
		
	}
	public void setId(String id){
		this.id = id;
	}
	public String getId(){
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getRetailer() {
		return retailer;
	}
	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
	
	public double getMicrosoftWarranty(){
	return 50.00;
}

public double getAppleWarranty(){
	return 60.00;
}

public double getSamsungWarranty(){
	return 30.00;
}
public String getType(){
	return type;
}
}
