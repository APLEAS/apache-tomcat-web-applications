package products;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Helper {
	HttpServletRequest req;
	PrintWriter pw;
	String url;
	HttpSession session; 


	public Helper(HttpServletRequest req) {
		this.req = req;
		this.session = req.getSession(true);
		 
	}


	
	public void logout(){
		session.removeAttribute("username");
		session.removeAttribute("usertype");
	}
	
	public boolean isLoggedin(){
		if (session.getAttribute("username")==null)
			return false;
		return true;
	}
	
	public String username(){
		if (session.getAttribute("username")!=null)
			return session.getAttribute("username").toString();
		return null;
	}
	
	public String usertype(){
		if (session.getAttribute("usertype")!=null)
			return session.getAttribute("usertype").toString();
		return null;
	}
	
	public User getUser(){
		String usertype = usertype();
		HashMap<String, User> hm = new HashMap<String, User>();
		if (usertype.equals("customer")) {
			hm.putAll(UserHashMap.customer);
		} else if (usertype.equals("retailer")) {
			hm.putAll(UserHashMap.retailer);
		} else if (usertype.equals("manager")) {
			hm.putAll(UserHashMap.manager);
		}
		User user = hm.get(username());
		return user;
	}
	
	public ArrayList<OrderItem> getCustomerOrders(){
		ArrayList<OrderItem> order = new ArrayList<OrderItem>(); 
		if(OrdersHashMap.orders.containsKey(username()))
			order= OrdersHashMap.orders.get(username());
		return order;
	}
		public ArrayList<OrderItem> getUserOrders(String u){
		ArrayList<OrderItem> order = new ArrayList<OrderItem>(); 
		if(OrdersHashMap.orders.containsKey(u))
			order= OrdersHashMap.orders.get(u);
		return order;
	}
	

	
	public int CartCount(){
		if(isLoggedin())
		return getCustomerOrders().size();
		return 0;
	}
	
	

		
	public void storeProduct(String name,String type,String maker, String acc, String cid){
		int id = Integer.parseInt(session.getAttribute("id").toString());
		System.out.println("this is the user id  "  + id);
		System.out.println("name   ="  + name);
		System.out.println("type   ="  + type);
		System.out.println("cid   ="  + cid);
	
		if(!OrdersHashMap.orders.containsKey(username())){	
			ArrayList<OrderItem> arr = new ArrayList<OrderItem>();
			OrdersHashMap.orders.put(username(), arr);
		}
		
		ArrayList<OrderItem> orderItems = OrdersHashMap.orders.get(username());
		
		if(type.equalsIgnoreCase("console")){
			HashMap<String, GameConsole> cMap  = ProductHashMap.consoles;
			 for(Map.Entry<String,GameConsole> map :cMap.entrySet()){
				 GameConsole c = map.getValue();
				 System.out.println(map.getKey() + "  :  " + map.getValue());
				 
				 System.out.println("c   name   " + c.getName());
			 }
			GameConsole console = cMap.get(cid);
			System.out.println("console name   =  "  + console.getName() );
			System.out.println(console.getPrice());
			OrderItem orderitem = new OrderItem(console.getName(), console.getPrice(), console.getImage(), console.getRetailer(), console.getType(), id);
			orderItems.add(orderitem);
		}
		if(type.equalsIgnoreCase("games")){
			Game game = null;
			if(maker.equalsIgnoreCase("electronicArts")){
				game = GameHashMap.electronicArts.get(name);
			}
			else if(maker.equalsIgnoreCase("activision")){
				game = GameHashMap.activision.get(name);
			}
			else if(maker.equalsIgnoreCase("takeTwoInteractive")){
				game = GameHashMap.takeTwoInteractive.get(name);
			}else{
				HashMap<String, Game> hm = new HashMap<String, Game>();
				hm.putAll(GameHashMap.electronicArts);
				hm.putAll(GameHashMap.activision);
				hm.putAll(GameHashMap.takeTwoInteractive);				
				game = hm.get(name);
			}
			
			
			OrderItem orderitem = new OrderItem(game.getName(), game.getPrice(), game.getImage(), game.getRetailer(), game.getType(), id);
			orderItems.add(orderitem);
		}
		
		if(type.equalsIgnoreCase("tablet")){
			HashMap<String, Tablet> cMap = ProductStore.getTabletMap();
			Tablet tablet = new Tablet();
			tablet = cMap.get(name);
			System.out.println("tablet   id   =   "  + cMap.size());
			OrderItem orderitem = new OrderItem(name, tablet.getPrice(), tablet.getImage(), tablet.getRetailer(), tablet.getType(), id);
			orderItems.add(orderitem);
		}
		
		if(type.equalsIgnoreCase("accessories")){
			GameConsole console = null;
			if(maker.equalsIgnoreCase("microsoft")){
				console = ConsoleHashMap.microsoft.get(acc);
				System.out.println("got my 360 "  + acc);
			}
			else if(maker.equalsIgnoreCase("sony")){
				console = ConsoleHashMap.sony.get(acc);
			}
			else if(maker.equalsIgnoreCase("nintendo")){
				console = ConsoleHashMap.nintendo.get(acc);
			}
				System.out.println("got inside accessory 1"  + name);
			Accessory accessory = console.getAccessory(name);
			 	System.out.println("got inside accessory 2  "   + accessory.getName());
			OrderItem orderitem = new OrderItem(accessory.getName(), accessory.getPrice(), accessory.getImage(), accessory.getRetailer(), accessory.getType(), id);
				System.out.println("got inside accessory 3");
			orderItems.add(orderitem);
		}
		
	}
	
	public String currentDate(){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/YYYY");
		Date date = new Date();
		return dateFormat.format(date).toString(); 
	}
	
	public HashMap<String, GameConsole> getConsoles(){
			HashMap<String, GameConsole> hm = new HashMap<String, GameConsole>();
			hm.putAll(ConsoleHashMap.microsoft);
			hm.putAll(ConsoleHashMap.sony);
			hm.putAll(ConsoleHashMap.nintendo);
			return hm;
	}
	
	public HashMap<String, Game> getGames(){
		HashMap<String, Game> hm = new HashMap<String, Game>();
			hm.putAll(GameHashMap.electronicArts);
			hm.putAll(GameHashMap.activision);
			hm.putAll(GameHashMap.takeTwoInteractive);
			return hm;
	}
	
	public HashMap<String, Tablet> getTablets(){
			HashMap<String, Tablet> hm = new HashMap<String, Tablet>();
			hm.putAll(TabletHashMap.apple);
			hm.putAll(TabletHashMap.microsoft);
			hm.putAll(TabletHashMap.samsung);
			return hm;
	}
	
	public ArrayList<String> getProducts(){
		ArrayList<String> ar = new ArrayList<String>();
		for(Map.Entry<String, GameConsole> entry : getConsoles().entrySet()){			
			ar.add(entry.getValue().getName());
		}

		return ar;
	}
	
	public ArrayList<String> getProductsGame(){		
		ArrayList<String> ar = new ArrayList<String>();
		for(Map.Entry<String, Game> entry : getGames().entrySet()){
			ar.add(entry.getValue().getName());
		}
		return ar;
	}
	
	public ArrayList<String> getProductsTablets(){		
		ArrayList<String> ar = new ArrayList<String>();
		for(Map.Entry<String, Tablet> entry : getTablets().entrySet()){
			ar.add(entry.getValue().getName());
		}
		return ar;
	}
	
	

}
