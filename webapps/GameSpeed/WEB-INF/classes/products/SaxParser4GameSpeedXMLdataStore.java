package products;

/*********


http://www.saxproject.org/

SAX is the Simple API for XML, originally a Java-only API. 
SAX was the first widely adopted API for XML in Java, and is a �de facto� standard. 
The current version is SAX 2.0.1, and there are versions for several programming language environments other than Java. 



The following URL from Oracle is the JAVA documentation for the API

https://docs.oracle.com/javase/7/docs/api/org/xml/sax/helpers/DefaultHandler.html


*********/





import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


////////////////////////////////////////////////////////////

/**************

SAX parser use callback function  to notify client object of the XML document structure. 
You should extend DefaultHandler and override the method when parsin the XML document

***************/

////////////////////////////////////////////////////////////

public class SaxParser4GameSpeedXMLdataStore extends DefaultHandler {
    GameConsole console;
    ArrayList<GameConsole> consoles;
	Accessory accessory;
	Product cProduct;
	Product aProduct;
	Product gProduct;
	Product tProduct;
	Game game;
	Tablet tablet;
	ArrayList<Accessory> accessories;
    String consoleXmlFileName = "ProductCatalog.xml";
    String elementValueRead;

    
    public SaxParser4GameSpeedXMLdataStore() {
      //  this.consoleXmlFileName = consoleXmlFileName;
        consoles = new ArrayList<GameConsole>();
		accessories = new ArrayList<Accessory>();
        parseDocument();
    }


    private void parseDocument() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(consoleXmlFileName, this);
        } catch (ParserConfigurationException e) {
            System.out.println("ParserConfig error");
        } catch (SAXException e) {
            System.out.println("SAXException : xml not well formed");
        } catch (IOException e) {
            System.out.println("IO error" + e.getMessage());
        }
    }






////////////////////////////////////////////////////////////

/*************

There are a number of methods to override in SAX handler  when parsing your XML document :

    Group 1. startDocument() and endDocument() :  Methods that are called at the start and end of an XML document. 
    Group 2. startElement() and endElement() :  Methods that are called  at the start and end of a document element.  
    Group 3. characters() : Method that is called with the text content in between the start and end tags of an XML document element.


There are few other methods that you could use for notification for different purposes, check the API at the following URL:

https://docs.oracle.com/javase/7/docs/api/org/xml/sax/helpers/DefaultHandler.html

***************/

////////////////////////////////////////////////////////////

    @Override
    public void startElement(String str1, String str2, String elementName, Attributes attributes) throws SAXException {

        if (elementName.equalsIgnoreCase("console")) {
			cProduct = new Product();
            console = new GameConsole();
            console.setId(attributes.getValue("id"));
            console.setRetailer(attributes.getValue("retailer"));
        }
		if(elementName.equalsIgnoreCase("accessory")){
			aProduct = new Product();
			accessory = new Accessory();
			accessory.setRetailer(attributes.getValue("retailer"));
			
		}
			if(elementName.equalsIgnoreCase("game")){
				gProduct = new Product();
			game = new Game();
			game.setRetailer(attributes.getValue("retailer"));
			game.setId(attributes.getValue("id"));
			
		}
			if(elementName.equalsIgnoreCase("tablet")){
			tProduct = new Product();
			tablet = new Tablet();
			tablet.setRetailer(attributes.getValue("retailer"));
			tablet.setId(attributes.getValue("id"));
			
		}

    }

    @Override
    public void endElement(String str1, String str2, String element) throws SAXException {
 
        if (element.equals("console")) {
			ProductHashMap.consoles.put(console.getId(), console);
            consoles.add(console);	
			cProduct.setRetailer(console.getRetailer());
			cProduct.setName(console.getName());
			cProduct.setType(console.getType());
			cProduct.setId(console.getId());
			cProduct.setPrice(console.getPrice());
			cProduct.setImage(console.getImage());
			AllProductsMap.products.put(cProduct.getName(), cProduct);
			//ProductStore.storeConsole(console, false);
	    return;
        }
		     if (element.equals("accessory")) {
			ProductHashMap.accessories.put(accessory.getId(), accessory);
            accessories.add(accessory);	
            console.addAccessory(accessory);
			aProduct.setRetailer(accessory.getRetailer());
			aProduct.setName(accessory.getName());
			aProduct.setType(accessory.getType());
			aProduct.setId(accessory.getId());
			int aPrice = (int)accessory.getPrice();
			aProduct.setPrice(aPrice);
			aProduct.setImage(accessory.getImage());
			AllProductsMap.products.put(aProduct.getName(), aProduct);
			ProductStore.storeAccessory(accessory, false);
	    return;
        }
		
		if (element.equals("game")) {
				ProductHashMap.games.put(game.getId(), game);
			gProduct.setRetailer(game.getRetailer());
			gProduct.setName(game.getName());
			gProduct.setType(game.getType());
			gProduct.setId(game.getId());
			int gPrice = (int)game.getPrice();
			gProduct.setImage(game.getImage());
			gProduct.setPrice(gPrice);
			AllProductsMap.products.put(gProduct.getName(), gProduct);
			ProductStore.storeGame(game, false);
	    return;
        }
				if (element.equals("tablet")) {
					System.out.println("storing tablet nme   =    "   + tablet.getName());
						ProductHashMap.tablets.put(tablet.getId(), tablet);
			tProduct.setRetailer(tablet.getRetailer());
			tProduct.setName(tablet.getName());
			tProduct.setType(tablet.getType());
			tProduct.setId(tablet.getId());
			tProduct.setImage(tablet.getImage());
			tProduct.setPrice(tablet.getPrice());
			AllProductsMap.products.put(tProduct.getName(), tProduct);
			ProductStore.storeTablet(tablet, false);
	    return;
        }
		
        if (element.equalsIgnoreCase("image")) {
            console.setImage(elementValueRead);
	    return;
        }
        if (element.equalsIgnoreCase("name")) {
            console.setName(elementValueRead);
	    return;
        }
      
        if(element.equalsIgnoreCase("price")){
            console.setPrice(Integer.parseInt(elementValueRead));
	    return;
        }
		
				if (element.equalsIgnoreCase("accessoryname")) {
            accessory.setName(elementValueRead);
	    return;
        }
		
		if (element.equalsIgnoreCase("accessoryPrice")) {
            accessory.setPrice(Double.parseDouble(elementValueRead));
	    return;
        }
		
			if (element.equalsIgnoreCase("accessoryImage")) {
            accessory.setImage(elementValueRead);
	    return;
        }
					if (element.equalsIgnoreCase("accessoryCondition")) {
            accessory.setCondition(elementValueRead);
	    return;
        }
		
			if (element.equalsIgnoreCase("accessoryDiscount")) {
            accessory.setDiscount(Double.parseDouble(elementValueRead));
	    return;
        }
			if (element.equalsIgnoreCase("gameimage")) {
            game.setImage(elementValueRead);
	    return;
        }
					if (element.equalsIgnoreCase("gamename")) {
            game.setName(elementValueRead);
	    return;
        }
						if (element.equalsIgnoreCase("gamecondition")) {
            game.setCondition(elementValueRead);
	    return;
        }
		
								if (element.equalsIgnoreCase("gameprice")) {
           game.setPrice(Integer.parseInt(elementValueRead));
	    return;
        }
		
									if (element.equalsIgnoreCase("gamediscount")) {
           game.setDiscount(Integer.parseInt(elementValueRead));
	    return;
        }
		
					if (element.equalsIgnoreCase("tabletimage")) {
            tablet.setImage(elementValueRead);
	    return;
        }
					if (element.equalsIgnoreCase("tabletname")) {
            tablet.setName(elementValueRead);
	    return;
        }
						if (element.equalsIgnoreCase("tabletcondition")) {
            tablet.setCondition(elementValueRead);
	    return;
        }
		
								if (element.equalsIgnoreCase("tabletprice")) {
           tablet.setPrice(Integer.parseInt(elementValueRead));
	    return;
        }
		
									if (element.equalsIgnoreCase("tabletdiscount")) {
           tablet.setDiscount(Integer.parseInt(elementValueRead));
	    return;
        }

    }


    @Override
    public void characters(char[] content, int begin, int end) throws SAXException {
        elementValueRead = new String(content, begin, end);
    }


    /////////////////////////////////////////
    // 	     Kick-Start SAX in main       //
    ////////////////////////////////////////

    public static void main(String[] args) {
        new SaxParser4GameSpeedXMLdataStore();

    }

}
