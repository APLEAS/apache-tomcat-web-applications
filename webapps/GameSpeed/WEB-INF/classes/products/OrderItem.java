package products;
import java.util.Date;

public class OrderItem implements java.io.Serializable {
	private int userId;
	private String name;
	private double price;
	private String image;
	private String retailer;
	private String type;
	public Date dateOrdered;
	public String tracking;
	public Date deliveryDate;
	public double warranty;
	
	public OrderItem(String name, double price, String image, String retailer, String type, int id){
		this.name=name;
		this.price=price;
		this.image=image;
		this.retailer = retailer;
		this.type=type;
		this.userId = id;
	}

	public int getId(){
	return userId;  
}
	
	
public void setDateOrdered(Date date){
	dateOrdered = date;  
}
public Date getDateOrdered(){
	return dateOrdered;
}
	public String getName() {
		return name;
	}
	
	public void setTracking(String t){
		this.tracking = t;
	}
	
	public String getTracking(){
		return tracking;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		
		return price;
			
	}
	
	public String getType(){
	return type;
	}
	public void setType(String type){
		this.type = type;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}
	
		public void setDeliveryDate(Date d){
		this.deliveryDate = d;
		
	}
	public boolean hasWarranty(){
		boolean war = false;
		if(warranty == 0){
			war = false;
		}else{
			war = true;
		}
		return war;
	}
	
	public Date getDeliveryDate(){
		return deliveryDate;
		
	}
		public double getWarranty(){
		return warranty;
		
	}
		public void setWarranty(double price){
		this.warranty = price;
		
	}
}
