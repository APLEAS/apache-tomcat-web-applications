package products;
import java.util.*;
import java.io.*;
import java.io.File;

public class ReviewStore{

	public static HashMap<String, Console> consoles = new HashMap<String, Console>();

// UserInfo


public static boolean storeReview(Review review, boolean update) {
	ArrayList<Review> reviews = new ArrayList<Review>();
	boolean store = false;
	System.out.println("storing a review  with the review name  "  + review.name);
 HashMap<String,ArrayList<Review>> mapInFile;
   
    try{
        File reviewData=new File("ReviewData");
		BufferedReader br = new BufferedReader(new FileReader("ReviewData"));
if(reviewData.exists() && !reviewData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(reviewData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,ArrayList<Review>>)ois.readObject();	
        ois.close();
        fis.close();
}else{ 
reviewData.createNewFile();
mapInFile = new HashMap<String, ArrayList<Review>>();
reviews.add(review);
mapInFile.put(review.name, reviews);
	store = true;
}	
			if(mapInFile.get(review.name) == null){
				reviews.add(review);
				store = true;
			mapInFile.put(review.name, reviews);
			} else{
reviews = mapInFile.get(review.name);
store = true;
for (Review r: reviews){
	if(r.userName.equals(review.userName)){
		store = false;
	System.out.println("you cannot review the same product twice  ");
	}
	
}
if(store){
	reviews.add(review);
mapInFile.put(review.name, reviews);
}
				
			;}
			
			if(store){
			try{	
		FileOutputStream fos=new FileOutputStream(reviewData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write Review to file ..." + e);
	}
			}
    }catch(Exception e){System.out.println("Could NOT write Review to file ..." + e);}
	
return store;
}

public static HashMap<String, ArrayList<Review>> getReview(){
 HashMap<String ,ArrayList<Review>> mapInFile = new HashMap<String, ArrayList<Review>>();
	 try{
        File reviewData=new File("ReviewData");
		BufferedReader br = new BufferedReader(new FileReader("ReviewData"));
if(reviewData.exists() && !reviewData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(reviewData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,ArrayList<Review>>)ois.readObject();	
        ois.close();
        fis.close();
}
	 }catch(Exception e){System.out.println("Could NOT review to file ..." + e);}
	 System.out.println("review map size   =   "  + mapInFile.size());
	return mapInFile;
}
}

