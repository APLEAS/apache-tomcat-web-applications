package products;
import java.util.ArrayList;
import java.util.List;


public class GameConsole implements java.io.Serializable{
    private String retailer;
    private String name;
    private String id;
    private String image;
    private String condition;
    private int price;
	private final String type = "console";
    private ArrayList<Accessory> accessories;
	private int discount;

public GameConsole(){
	this.accessories = new ArrayList<Accessory>();
}
public void setId(String id) {
	this.id = id;
}
public String getId(){
	return id;
}

public void setRetailer(String retailer) {
	this.retailer = retailer;
}


public void setImage(String image) {
	this.image = image;
}

public void setCondition(String condition) {
	this.condition = condition;
}

public void setPrice(int price) {
	this.price = price;
}

public ArrayList<Accessory> getAccessories() {
	return accessories;
}

public Accessory getAccessory(String name) {
	for (Accessory a : accessories){
		System.out.println(a.name);
		if(name.equals(a.getName())){
			return a;
	}
	}
	return null;
		
}


public void setName(String name) {
	this.name = name;
}

public void addAccessory(Accessory a) {
	accessories.add(a);
}
public String getName(){
	return name;
}
public String getRetailer(){
	return retailer;
}
public int getPrice(){
	return price;
}
public String getImage(){
	return image;
}
public double getMicrosoftWarranty(){
	return 50.00;
}

public double getSonyWarranty(){
	return 45.00;
}

public double getNintendoWarranty(){
	return 40.00;
}
public String getType(){
	return type;
}
public int getDiscount(){
	return discount;	
}

public void setDiscount(int discount){
	this.discount = discount;  
}

public String getCondition(){
	return condition;	
}



}

