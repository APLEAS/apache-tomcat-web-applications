package products;

import java.util.*;
import java.io.*;
import java.io.File;
public class UserStore{


// The known platforms

	public static HashMap<String, User> users = new HashMap<String, User>();







// UserInfo

// Read the HashMaps from the File GameSpeedDataStore

public static boolean storeUser(User user) {
	
boolean store = false;
 HashMap<String,User> mapInFile;
   
    try{
        File userData=new File("UserData");
		BufferedReader br = new BufferedReader(new FileReader("UserData"));
if(userData.exists() && !userData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(userData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,User>)ois.readObject();	
        ois.close();
        fis.close();
}else{ 
userData.createNewFile();
mapInFile = new HashMap<String, User>();
mapInFile.put(user.getName(), user);
	store = true;
}
   
		

		
			if(mapInFile.get(user.getName()) == null){
				store = true;
				
			mapInFile.put(user.getName(), user);
			} else{  System.out.println("this user is allready in the system  "  + user.getName());
			store = true;}
			
			if(true){
			try{
SQLStoreUsers uStore= new SQLStoreUsers();
uStore.addUser(user.getId(), user.getName(), user.getPassword(), user.getType());
			
		FileOutputStream fos=new FileOutputStream(userData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write user to file ..." + e);
	}
			}
	
        
        for(Map.Entry<String,User> m :mapInFile.entrySet()){
            	System.out.println(m.getKey());
		User c = m.getValue();
		System.out.println("\t Name : "+c.getName());
		System.out.println("\t Password : "+c.getPassword());
		System.out.println("\t type : "+c.getUsertype());
		System.out.println("\t id : "+c.getId());

        }
		System.out.println("size of user hashmap  =   " + mapInFile.size());
    }catch(Exception e){System.out.println("Could NOT read user to file ..." + e);}
	
return store;
}

public static int getNextId() {
int count = 0;
int next = 0;
	if(getAllUsers() != null){
HashMap<String, User> mapInFile = getAllUsers();
int[] orderedId = new int[mapInFile.size()];

if(mapInFile.size() > 0 && mapInFile != null){
	 for(Map.Entry<String,User> m :mapInFile.entrySet()){
            	User u = m.getValue();
		orderedId[count] = u.getId();  
		count+=1;
        }
		Arrays.sort(orderedId);
		next = orderedId[orderedId.length -1];
}
	}
		return next;
		
		
	 }



public static HashMap<String,User> getAllUsers() {
 HashMap<String,User> mapInFile;  
    try{
        File userData=new File("UserData");
		BufferedReader br = new BufferedReader(new FileReader("UserData"));
if(userData.exists() && !userData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(userData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,User>)ois.readObject();	
        ois.close();
        fis.close();
		return mapInFile;
}

    }catch(Exception e){System.out.println("Could NOT read user file ..." + e);}
	
return null;
}


public static boolean verifyUser(String name, String password, String type){
		boolean verify = false;
 HashMap<String,User> mapInFile = new HashMap<String, User>();
	 try{
        File userData=new File("UserData");
		BufferedReader br = new BufferedReader(new FileReader("UserData"));
if(userData.exists() && !userData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(userData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,User>)ois.readObject();	
        ois.close();
        fis.close();
}
	 }catch(Exception e){System.out.println("Could NOT read user to file ..." + e);}
	 if(mapInFile.get(name) != null){
	 User u = mapInFile.get(name);
	if(name.equals(u.getName()) && password.equals(u.getPassword()) && type.equals(u.getUsertype())){
		verify = true;
	}
	 }
	return verify;
}

public static boolean deleteUser(String name, String password, String type) {
	
boolean verify = false;
 HashMap<String,User> mapInFile = new HashMap<String, User>();
   
    try{
        File userData=new File("UserData");
		BufferedReader br = new BufferedReader(new FileReader("UserData"));
if(userData.exists() && !userData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(userData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,User>)ois.readObject();	
        ois.close();
        fis.close();
}	 if(mapInFile.get(name) != null){
	 User u = mapInFile.get(name);
			if(name.equals(u.getName()) && password.equals(u.getPassword()) && type.equals(u.getUsertype())){
				mapInFile.remove(name);
		verify = true;
	}
}
			
			if(verify){
			try{	
		FileOutputStream fos=new FileOutputStream(userData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write user to file ..." + e);
	}
			}
	
        
        for(Map.Entry<String,User> m :mapInFile.entrySet()){
            	System.out.println(m.getKey());
		User c = m.getValue();
		System.out.println("\t Name : "+c.getName());
		System.out.println("\t Password : "+c.getPassword());
		System.out.println("\t type : "+c.getUsertype());

        }
		System.out.println("size of user hashmap  =   " + mapInFile.size());
    }catch(Exception e){System.out.println("Could NOT read user to file ..." + e);}
	
return verify;
}

 public static void main(String[] args) {
	User u1 = new User("Sally", "dfsfg", "customer");
	User u2 = new User("Brendon", "nakedgun1", "customer");
	storeUser(u1);
	storeUser(u2);
	
	
}

}