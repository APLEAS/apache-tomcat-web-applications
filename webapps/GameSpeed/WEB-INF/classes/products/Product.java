package products;
import java.util.ArrayList;
import java.util.List;


public class Product implements java.io.Serializable{
    private String retailer;
    private String name;
    private String id;
    private String type;
	private int price;
	private String image;
	
	Product(){
		
	}
	
	public void setImage(String image){
		this.image = image;
	}
		public String getImage(){
		return image;
	}
	public void setPrice(int price){
		this.price = price;
	}
		public int getPrice(){
		return price;
	}
  
public void setId(String id) {
	this.id = id;
}
public String getId(){
	return id;
}

public void setRetailer(String retailer) {
	this.retailer = retailer;
}


public void setName(String name) {
	this.name = name;
}


public String getName(){
	return name;
}
public String getRetailer(){
	return retailer;
}

public void setType(String type){
	this.type = type;
}

public String getType(){
	return type;
}






}

