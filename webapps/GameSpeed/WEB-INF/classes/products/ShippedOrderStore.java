package products;
import java.util.*;
import java.io.*;
import java.io.File;
public class ShippedOrderStore{


// The known platforms

	


// UserInfo

// Read the HashMaps from the File GameSpeedDataStore

public static boolean shipOrder(String user, ArrayList<OrderItem> items, boolean cancel, String orderName) {
	System.out.println("Inside ShipperOrderStore shipOrder");
	for (OrderItem o : items){
		Date date = new Date();
		o.setDateOrdered(date);
	}
	boolean store = false;
	ArrayList<OrderItem> orderList= new ArrayList<OrderItem>();
 HashMap<String,ArrayList<OrderItem>> mapInFile;
   
    try{
        File shippedData=new File("ShipData");
		BufferedReader br = new BufferedReader(new FileReader("ShipData"));
if(shippedData.exists() && !shippedData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(shippedData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,ArrayList<OrderItem>>)ois.readObject();	
        ois.close();
        fis.close();
}else{ 
mapInFile = new HashMap<String, ArrayList<OrderItem>>();

mapInFile.put(user, items);
	store = true;
}
   
			if(cancel){
				System.out.println("inside cancel 1");
			orderList = mapInFile.get(user);
			System.out.println("orderlist   length1   =   "  + orderList.size());
			for (int j = 0; j < orderList.size(); j++){
				System.out.println("cancel order name   " + orderList.get(j).getName());
				System.out.println("Order to cancel   " + orderName);
				Date d = new Date();
				long diff = d.getTime() - orderList.get(j).getDateOrdered().getTime();
				int diffDays = (int) (diff/(24 * 60 * 60 * 1000));
				if(orderList.get(j).getName().equals(orderName) && diffDays < 9){
					System.out.println("inside cancel 2");
					orderList.remove(j);
					System.out.println("orderlist   length2   =   "  + orderList.size());
					mapInFile.put(user, orderList);
					
					store = true;
				}
			}
			
			
		}
			if(mapInFile.get(user) == null && !cancel){
				store = true;
			mapInFile.put(user, items);
			} else if (!cancel){ orderList = mapInFile.get(user);
			System.out.println("inside else 1 "  + orderList.size());
			orderList.addAll(items);
			System.out.println("inside else "  + orderList.size());
			mapInFile.put(user, orderList);
			
			
			store = true;
			}
			
			if(store){
			try{	
		FileOutputStream fos=new FileOutputStream(shippedData, false);
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(mapInFile);
        oos.flush();
        oos.close();
        fos.close();
			    }catch(Exception e){
		System.out.println("Could NOT Write user to file ..." + e);
	}
			}
    }catch(Exception e){System.out.println("Could NOT read user to file ..." + e);}
	
return store;
}

public static ArrayList<OrderItem> getShippedOrders(String user) {
 HashMap<String,ArrayList<OrderItem>> mapInFile;
 mapInFile = new HashMap<String, ArrayList<OrderItem>> ();
    try{
        File shippedData=new File("ShipData");
		BufferedReader br = new BufferedReader(new FileReader("ShipData"));
//if(shippedData.exists() && !shippedData.isDirectory() && br.readLine() != null) { 
       FileInputStream fis=new FileInputStream(shippedData);
        ObjectInputStream ois=new ObjectInputStream(fis);
      mapInFile=(HashMap<String,ArrayList<OrderItem>>)ois.readObject();	
        ois.close();
        fis.close();
		
		ArrayList<OrderItem> orders = mapInFile.get(user);
		System.out.println(orders.size());
		return orders;


 }catch(Exception e){System.out.println("Could NOT read user to file ..." + e);}


return mapInFile.get(user);

}
}