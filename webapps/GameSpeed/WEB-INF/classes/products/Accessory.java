package products;


public class Accessory implements java.io.Serializable {
	 String name;
	 double price;
	 String image;
	 String retailer;
	 String condition;
	 double discount;
	 String id;
	 String type;
	
	public Accessory (){
	}
	

	public String getName() {
		return name;
	}
	 public void setId(String id){
		this.id = id;
		
	}
	public String getId(){
		return id;
		
	}

	public void setName(String name) {
		this.name = name;
	}

	 public double getPrice() {
		return price;
	}

	 public void setPrice(double price) {
		this.price = price;
	}

	 public String getImage() {
		return image;
	}

	 public void setImage(String image) {
		this.image = image;
	}

	 public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public String getType(){
		return "accessory";
	}
	

}
