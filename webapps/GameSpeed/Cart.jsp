<HTML>

  <%-- import any packages needed by the page --%>
    <%@ page import="products.*, products.ConsoleHashMap,products.ProductStore, java.util.Map,java.util.HashMap, java.io.*, java.util.*" %>

<%
		Helper helper = new Helper(request);
		String name = "";
		String type = "";
		String maker = "";
		String access = "";
		String oid = "";
		boolean newOrder = true;
		if(request.getParameter("name") !=null && request.getParameter("name") != ""){
			name = request.getParameter("name");
		}else{newOrder = false;}
				if(request.getParameter("type") !=null && request.getParameter("type") != ""){
			type = request.getParameter("type");
		}else{newOrder = false;}
			if(request.getParameter("maker") !=null && request.getParameter("maker") != ""){
		maker = request.getParameter("maker");
		}else{newOrder = false;}
			if(request.getParameter("access") !=null && request.getParameter("access") != ""){
		access = request.getParameter("access");		
		}
		
		if(true)
			System.out.println("name: " + name);
		System.out.println("type: " +type);
		System.out.println("maker: " +maker);
	 oid = request.getParameter("oid");
		helper.storeProduct(name, type, maker, name, oid);
		
				if(!helper.isLoggedin()){			
			session.setAttribute("login_msg", "Please Login to add items to cart");
			response.sendRedirect("Login.jsp");
			return;
		}
		

	

%>

<%@include file="global/site_header.html"%>




  <%@include file="global/site_sidebar.html"%>
<div id='content'><div class='post'><h2 class='title meta'>
		<a style='font-size: 24px;'>Cart(<%=helper.CartCount()%>)</a>
		</h2><div class='entry'>
		
	<%	if(helper.CartCount()>0){ %>
		<table  class='gridtable'>
		<tr>	<td>#</td><td>Item Name</td><td>Item Price</td><td>Warranty
			</td> </tr>
		<%int i = 1;
		double total = 0;
		double wPrice = 0;
		ArrayList<OrderItem> order = new ArrayList<OrderItem>();
		GameConsole gCon = new GameConsole();
		Tablet t = new Tablet();
		Game g = new Game();
		for (OrderItem o : helper.getCustomerOrders()) {
			
			if(o.getType().equalsIgnoreCase("console")){
			if(o.getRetailer().equalsIgnoreCase("microsoft")){
				wPrice = gCon.getMicrosoftWarranty();
			}
			if(o.getRetailer().equalsIgnoreCase("sony")){
				wPrice = gCon.getSonyWarranty();
			}
			if(o.getRetailer().equalsIgnoreCase("microsoft")){
				wPrice = gCon.getNintendoWarranty();
			}
			}
			
			if(o.getType().equalsIgnoreCase("game")){
			if(o.getRetailer().equalsIgnoreCase("electronic arts")){
				
				wPrice = g.getEAWarranty();
				System.out.println("warranty for ea   "  + wPrice);
			}
			if(o.getRetailer().equalsIgnoreCase("activision")){
				wPrice = g.getActivisionWarranty();
			}
			if(o.getRetailer().equalsIgnoreCase("Take-Two Interactive")){
				wPrice = g.getTake2Warranty();
			}
			}
			
					if(o.getType().equalsIgnoreCase("tablet")){
			if(o.getRetailer().equalsIgnoreCase("apple")){
					wPrice = t.getAppleWarranty();
			}
			if(o.getRetailer().equalsIgnoreCase("Microsoft")){
			
				wPrice = t.getMicrosoftWarranty();
			}
			if(o.getRetailer().equalsIgnoreCase("Samsung")){
				wPrice = t.getSamsungWarranty();
			}
			}
			order.add(o);
%>
			
			<tr>	<td><%=i%>.</td><td><%=o.getName()%></td><td>$<%=o.getPrice()%>
			</td>
			<%if (!o.hasWarranty()){  %>
			<td><form method='post' action='Warranty.jsp'><input type='submit' class='btnbuy' name='warranty' value='Add Warranty' </input>
			<input type='hidden' name='wprice' value=<%=wPrice%> </input>
			<input type='hidden' name='maker' value=<%=o.getRetailer()%> </input>
			<input type='hidden' name='type' value=<%=o.getType()%></input>
			<input type='hidden' name='name' value=<%=o.getName()%>></input>
			</td>
			</form>
			<%}else{ %>
			<td><%=o.getWarranty()%></td>
			<%}%>
		    
			<td><form method='post' action='DeleteItem.jsp'><input type='submit' class='btnbuy' name='delete' value='Delete' </input>
			<input type='hidden' name='name' value='<%=o.getName()%>' </input>
			<input type='hidden' name='maker' value=<%=o.getRetailer()%> </input>
			<input type='hidden' name='type' value=<%=o.getType()%></input>
			<input type='hidden' name='order' value=<%=o.getName()%>></input>
				<input type='hidden' name='oid' value=<%=oid%>></input>
			</td>
			</form>
			</tr>
			
			<%
			total = total +o.getPrice() +o.getWarranty();
			i++;
		}%>

		<tr><th></th><th>Total</th><th><%=total%></th>
		
		<tr><td></td><td><form method='get' action='CheckOut.jsp'>
		<input type='submit' class='btnbuy' name='checkout' value='Check Out'</input>
		<input type='hidden', name='price' value=<%=total%></input></td></form>
		<tr><td></td>
		</table>
		<%
		}else{ %>
			<h4 style='color:red'>Your Cart is empty</h4>
		<%}
		%>
		
		</div></div></div>		
		

</HTML>
